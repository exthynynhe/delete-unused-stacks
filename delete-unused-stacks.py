#!/usr/bin/python3

import boto3
from datetime import datetime, timedelta
import pytz

STATUS_SET = ("CREATE_COMPLETE","UPDATE_COMPLETE","ROLLBACK_COMPLETE","UPDATE_ROLLBACK_COMPLETE")

nextToken = None
client = boto3.client('cloudformation')
removables = []
utc = pytz.UTC
referenceTime = utc.localize(datetime.now() - timedelta(days=60))

# If RootId is present, stack is part of nested stack. Deleting root stack should delete its children.
# Some stacks are never updated and thus missing value 'LastUpdatedTime'. Using 'CreateTime' instead.
def crawlStacks(stacks):
    for stack in stacks["Stacks"]:
        if "RootId" in stack:
            continue
        if "LastUpdatedTime" in stack:
            updated = stack["LastUpdatedTime"]
            if updated <= referenceTime:
                removables.append(stack["StackName"])
        else:
            created = stack["CreationTime"]
            if created <= referenceTime:
                removables.append(stack["StackName"])

# Get CF stacks as long as there are stacks to get. API returns only 1MB objects, thus NextToken.
# crawlStack() will filter stacks to be removed and results are stored in global variable 'removables'.
# In the end all stacks in removables will be deleted. delete_stack() doesn't return anything.
def handler():
    nextToken = None
    stacks = client.describe_stacks()
    if 'NextToken' in stacks:
        nextToken = stacks["NextToken"]
    crawlStacks(stacks)
    while nextToken:
        stacks = client.describe_stacks(NextToken=nextToken)
        crawlStacks(stacks)
        if 'NextToken' in stacks:
            nextToken = stacks["NextToken"]
        else:
            break
    #print(str(referenceTime))
    #print(removable)
    for stack in removables:
        print("Deleting stack " + stack)
        #client.delete_stack(stack)
    print("Done")
    return 0
            
handler()